package it.hensemberger.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DBUtils {

	static{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() throws SQLException{
		return DriverManager.getConnection("jdbc:mysql://51.255.165.111/Test", "root", "Passw0rd");
	}
	
	public static void main(String[] args) {
		try {
			Connection con = getConnection();
			//PreparedStatement ps = con.prepareStatement("SELECT * FROM tabella WHERE id = ?");
			//ps.setInt(1, 789);
			//ResultSet rsResultSet = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
