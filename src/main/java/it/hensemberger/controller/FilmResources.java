package it.hensemberger.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mysql.jdbc.StringUtils;

import it.hensemberger.media.Film;
import it.hensemberger.persistence.FilmPersistenceManager;

/**
 * Servlet implementation class FilmResources
 */
public class FilmResources extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FilmPersistenceManager filmPersistenceManager = new FilmPersistenceManager();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FilmResources() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String paramsString = request.getPathInfo();
		List<String> params = StringUtils.split(paramsString, "/", true);
		
		if(params.size()==0){
			Gson gson = new Gson();
			List<String> films = filmPersistenceManager.getFilms();
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().append(gson.toJson(films));
		}
		if(params.size()==1){
			Film film = filmPersistenceManager.getFilm(Integer.parseInt(params.get(0)));
			if(film==null){
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}else{
				Gson gson = new Gson();
				response.setContentType("application/json;charset=UTF-8");
				response.getWriter().append(gson.toJson(film));
			}
		}
		if(params.size()>1){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String paramsString = request.getPathInfo();
		List<String> params = StringUtils.split(paramsString, "/", true);
		if(params.size()==0){
			String json = getBody(request);
			Gson gson = new Gson();
			Film film = gson.fromJson(json, Film.class);
			String uri = filmPersistenceManager.addFilm(film);
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().append(uri);
		}
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPut(req, resp);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doDelete(req, resp);
	}
	
	private static String getBody(HttpServletRequest request) throws IOException {

	    String body = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    BufferedReader bufferedReader = null;

	    try {
	        InputStream inputStream = request.getInputStream();
	        if (inputStream != null) {
	            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	            char[] charBuffer = new char[128];
	            int bytesRead = -1;
	            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	                stringBuilder.append(charBuffer, 0, bytesRead);
	            }
	        } else {
	            stringBuilder.append("");
	        }
	    } catch (IOException ex) {
	        throw ex;
	    } finally {
	        if (bufferedReader != null) {
	            try {
	                bufferedReader.close();
	            } catch (IOException ex) {
	                throw ex;
	            }
	        }
	    }

	    body = stringBuilder.toString();
	    return body;
	}

}
