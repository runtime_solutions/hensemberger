package it.hensemberger.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import it.hensemberger.media.Film;
import it.hensemberger.media.Genere;
import it.hensemberger.media.Review;

public class FilmPersistenceManager {
	private Map<Integer, Film> memory = new ConcurrentHashMap<Integer, Film>();
	
	public FilmPersistenceManager(){
		Film film = new Film();
		film.setId(1);
		film.setTitle("Fight Club");
		film.setType("FILM");
		
		Genere genere = new Genere();
		genere.setId(7);
		genere.setName("Drama");
		film.setGenere(genere);
		
		List<Review> reviews = new ArrayList<Review>();
		Review review = new Review();
		review.setRate(10);
		review.setComment("Probably best movie in the world");
		reviews.add(review);
		review = new Review();
		review.setRate(7);
		review.setComment("Good movie");
		reviews.add(review);
		review = new Review();
		review.setRate(8);
		review.setComment("Great movie");
		reviews.add(review);
		film.setReviews(reviews);
		memory.put(1, film);
	}
	
	public List<String> getFilms(){
		List<String> films = new ArrayList<String>();
		for (Iterator<Integer> iterator = memory.keySet().iterator(); iterator.hasNext();) {
			String string = "http://localhost:8080/Hensemberger/resources/film/" + iterator.next();
			films.add(string);
		}
		return films;
	}
	
	public Film getFilm(int id){
		return memory.get(id);
	}
	
	public synchronized String addFilm(Film film){
		int key = memory.size()+1;
		film.setId(key);
		memory.put(key, film);
		return "http://localhost:8080/Hensemberger/resources/film/"+key;
	}
}
