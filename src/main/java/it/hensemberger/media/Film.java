package it.hensemberger.media;

public class Film extends AbstractMedia {
	private Genere genere;

	public Genere getGenere() {
		return genere;
	}
	public void setGenere(Genere genere) {
		this.genere = genere;
	}
	
}
